When you first start the game, you will have an empty party.  A party is
nothing more than a group of pawns (also called "characters") assembled for the
purpose of adventuring in the "maze."

The Maze is so named because well, it's easy to get lost in a map like this
when it's really really big.  The demo map is not very big, and you should be
perfectly safe even if you do get lost.

BUT, the config file formats (and pawn formats, etc.) are broken routinely at
this early stage in development.  This is why the game will create new files
if there are no compatible ones to be found.

Before you can enter the maze, though, you have to have at least one pawn in
your party.  After all, you can't very well go exploring if there isn't
actually anybody to DO the exploring, right?

Default controls are for the Number Pad on your keyboard.  In MANY cases, the
Enter/Return key will double as an OK button, and Esc will double as a Cancel
button.  This is NOT ALWAYS TRUE, YET.

Number Pad Keys:
8 = Forward
5 = Backward
4 = Step Left
6 = Step Right
7 = Turn Left
9 = Turn Right
2 = Turn Around
Enter = "Ok" or "Act."  This button is used to get through doors.
0 = "Cancel."  This button is used for exiting menus, etc.

Arrow Keys: (in menu mode)
Up = Move cursor up one position
Down = Move cursor down
Left = Usually nothing.  If this key can be used, it will be noted onscreen.
Right = Usually nothing.  If this key can be used, it will be noted onscreen.

Begin by going to the edge of town, and then to the Training Grounds.  Create
at least one pawn.  Each pawn is given a random number of bonus points ranging
from 8 to 22.  Your chances of getting a lower number are MUCH greater than
getting a higher one.  The odds of getting a bonus of 8-12 are about 50%.  The
odds of getting the full bonus of 22% is less than one percent.  It's actually
something you *might* see about every 500 attempts.

The pawn you can create depends heavily on the choices you've made up to the
class selection.  For example, a Thief cannot be good, and a Cleric (priest)
cannot be neutral.  A Ninja (which you cannot create straight up) must be
evil, while a Samurai (which you cannot create straight up) can only be good or
neutral.

Theoretically, your party cannot be composed of mixed alignments, but currently
there is no code in place preventing this.  For example, you can mix a good
fighter with an evil cleric.  This "feature" will go away.

So create at least one pawn, and then go back to the Ranger's Respite Tavern.
Choose "Add" to add up to six pawns to your party, and you can now enter the
maze.

Press F12 to change the keybindings if they are not comfortable for you.

I STRONGLY recommend that you do not bind any keys to Enter or Esc, except
"ok" and "cancel," though, because if you do (and the game should not let you),
then it may behave oddly.

