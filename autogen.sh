#! /bin/bash

# locate sdl.m4, and copy it to the macros/ directory if it's not in aclocal's
# default include path.
export M4S=macros/
[ ! -d macros ] && mkdir macros
[ -f `aclocal --print-ac-dir`/sdl.m4 ] && \
				echo "sdl.m4 found in `aclocal --print-ac-dir`"

[ ! -f `aclocal --print-ac-dir`/sdl.m4 ] && {
    [ -f /usr/share/aclocal/sdl.m4 ] && cp -f /usr/share/aclocal/sdl.m4 macros
    [ -f /usr/local/share/aclocal/sdl.m4 ] && \
					   cp -f /usr/local/share/sdl.m4 macros
    [ ! -f macros/sdl.m4 ] && {
	echo "*** error: sdl.m4 not found."
	exit 1;
	};
};

aclocal -I $M4S \
&& automake --gnu --add-missing \
&& autoconf \
&& {
[ `whoami` == "sibn" ] && ./configure --prefix=$HOME/gwiz $*
[ ! `whoami` == "sibn" ] && ./configure $* 
} \
&& ls macros \
&& rm -rf ./macros


