/*  inspect.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_INSPECT_H
#define HAVE_INSPECT_H

/* prime the inspection system by precaching common images and variables */
void InitInspectEngine(GwizInspector *gi);

/* select a pawn to view (by mode) */
void InspectPawn (int mode);

/* just create the GI.  don't use it. */
void NewGwizInspector(PlayerPawn *pawn, int mode, int whichpawn);

/* set the coordinates for various destinations */
void SetInspectorOverlays (GwizInspector *gi);

/* set the labels for various fields; the overlay fits over it transparently */
void SetInspectorLabels(GwizInspector *gi);

/* The top row with generally useful information */
void SetInspectorGenerics(GwizInspector *gi, PlayerPawn *pawn);

/* Experience, Gold, Marks.  Things that have lots of space. */
void SetInspectorMajorCounters (GwizInspector *gi, PlayerPawn *pawn);

/* Age, AC, Swim, RIP. things that do not generally go very far. */
void SetInspectorMinorCounters (GwizInspector *gi, PlayerPawn *pawn);

/* Generic attributes. */
void SetInspectorAttrCounters (GwizInspector *gi, PlayerPawn *pawn);

/* HP, Magic. */
void SetInspectorOtherCounters (GwizInspector *gi, PlayerPawn *pawn);

/* Inventory */
void SetInspectorInventory (GwizInspector *gi, PlayerPawn *pawn);

/* Generic menu.  will create the correct menu depending on pawn->class */
void InspectPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode);

void InspectFighterPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode);

void InspectMagePawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode);

void InspectClericPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode);

void InspectWizardPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode);

void InspectThiefPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode);

void InspectSamuraiPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode);

void InspectLordPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode);

void InspectNinjaPawnMenu (GwizInspector *gi, PlayerPawn *pawn, int mode);

void GwizInspectorMajorCounterUpdate (GwizInspector *gi, PlayerPawn *pawn);

#endif /* HAVE_INSPECT_H */

