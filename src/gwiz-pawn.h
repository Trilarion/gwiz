/*  gwiz-pawn.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* Generally useful PlayerPawn related stuff.  Put it here so it's -easily-
   accessible by the rest of the game. */

#ifndef GWIZ_PAWN_H
#define GWIZ_PAWN_H

#include "spells.h"

#define FIGHTER 0
#define MAGE 1
#define CLERIC 2
#define THIEF 3
#define WIZARD 4
#define SAMURAI 5
#define LORD 6
#define NINJA 7

#define MALE 0
#define FEMALE 1

#define HUMAN 0
#define ELF 1
#define DWARF 2
#define GNOME 3
#define HOBBIT 4

#define GOOD 0
#define NEUTRAL 1
#define EVIL 2

#define CURSED        0x00000001

#define STATUS_OK     0x00000000
#define STATUS_POISON 0x00000001
#define STATUS_PETRIF 0x00000002
#define STATUS_DEAD   0x00000004
#define STATUS_ASH    0x00000008
#define STATUS_AFRAID 0x00000010
#define STATUS_LOST   0x00000020

#define USAGE_NONE       0x00000000
#define USAGE_SINGLE     0x00000001
#define USAGE_MULTIPLE   0x00000002
#define USAGE_EQUIPPABLE 0x00000004
#define USAGE_EQUIPPED   0x00000008
#define USAGE_SWORD      0x00000010
#define USAGE_ARMOR      0x00000020
#define USAGE_HELMET     0x00000040
#define USAGE_GAUNTLET   0x00000080
#define USAGE_BOOT       0x00000100
#define USAGE_RING       0x00000200
#define USAGE_SCROLL     0x00000400
#define USAGE_POTION     0x00000800
#define USAGE_MACE       0x00001000
#define USAGE_DAGGER     0x00002000
#define USAGE_BOW        0x00004000
#define USAGE_STAFF      0x00008000
#define USAGE_2X_SWORD   0x00010000
#define USAGE_ROBES      0x00020000
#define USAGE_SHIELD     0x00040000
#define USAGE_LEATHER    0x00080000
#define USAGE_AMULET     0x00100000
#define USAGE_NOEFFECT   0x00200000
#define USAGE_UNIDENT    0x00400000

#define MAXPAWNS 6

typedef enum {
    HUMAN_STR = 18,
    HUMAN_IQ = 18,
    HUMAN_DEV = 15,
    HUMAN_VIT = 18,
    HUMAN_AGI = 18,
    HUMAN_LUCK = 19,

    ELF_STR = 17,
    ELF_IQ = 20,
    ELF_DEV = 20,
    ELF_VIT = 16,
    ELF_AGI = 19,
    ELF_LUCK = 16,

    DWARF_STR = 20,
    DWARF_IQ = 17,
    DWARF_DEV = 20,
    DWARF_VIT = 20,
    DWARF_AGI = 15,
    DWARF_LUCK = 16,

    GNOME_STR = 17,
    GNOME_IQ = 17,
    GNOME_DEV = 20,
    GNOME_VIT = 18,
    GNOME_AGI = 20,
    GNOME_LUCK = 17,

    HOBBIT_STR = 15,
    HOBBIT_IQ = 17,
    HOBBIT_DEV = 17,
    HOBBIT_VIT = 16,
    HOBBIT_AGI = 22,
    HOBBIT_LUCK = 25
} MaxAttributes;

typedef struct MonsterPawn_ MonsterPawn;
typedef struct PlayerPawn_ PlayerPawn;
typedef struct PlayerAttributes_ PlayerAttributes;
typedef struct PlayerCounters_ PlayerCounters;
typedef struct PlayerMagic_ PlayerMagic;
typedef struct Inventory_ Inventory;
typedef struct PlayerItem_ Item;

#define ITEM_VERSION 1

struct PlayerItem_ {
    char name[16];
    int modifier;
    char effect[7];
    int cost;
    int when_useable;
};

struct Inventory_ {
    char name[16];
    int version;
    int acmod;
    int swimmod;
    int luckmod;
    int agimod;
    int vitmod;
    int devmod;
    int iqmod;
    int strmod;
    int special;
    int buyfor;
    int sellfor;
    int usage;
    int magice;  /* mage magic effect */
    int clerice; /* clerical magic effect */
    int itemno;
};

struct PlayerMagic_ {
    GwizSpell *spell;
    char ptlist[20];
    int spellpoints[7];
    int maxpoints[7];
};

struct PlayerCounters_ {
    int hp;
    int hp_max;
    unsigned long int ep;
    unsigned long int gp;
    unsigned long int nextlev;
    int marks;
    int age;
    int rip;
};

struct PlayerAttributes_ {
    int ac;
    int str;
    int iq;
    int dev;
    int vit;
    int agi;
    int luck;
    int swim;
};

struct PlayerPawn_ {
    PlayerAttributes attr;
    PlayerCounters counter;
    PlayerMagic mm;
    PlayerMagic cm;
    Inventory item[8];
    
    char name[16];
    int spellsknown[64];
    int level;
    int sex;
    int ali;
    int class;
    int race;
    int status;
    int saved_x; /* variables to save the player's position in the maze */
    int saved_y;
    int saved_z;
    int version;
};

struct MonsterPawn_ {
    SDL_Surface *visual;
    char name[16];
    char pixpath[13]; /* filename to pixmap. */
    int minphys;
    int maxphys;
    int mspells;
    int cspells;
    int hp;
    int epval;
    int gpval;
    int susceptibility;
    int specialphys;
};

#endif /* GWIZ_PAWN_H */
