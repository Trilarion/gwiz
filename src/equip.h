/*  equip.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_EQUIP_H
#define HAVE_EQUIP_H

/* The base function itself */
void Equip (GwizInspector *gi, PlayerPawn *pawn);

/* Equip item if possible, unequip if already equipped (and not cursed) */
void FlipEquippedState (GwizInspector *gi, PlayerPawn *pawn);

/* Fetch the position of the next item a pawn has that is equippable */
int GetNextEquippableItem (PlayerPawn *pawn);
int GetPrevEquippableItem (PlayerPawn *pawn);

void MoveEquipCursor (GwizInspector *gi, PlayerPawn *pawn, int goingup);

void EquipMenuLoop (GwizInspector *gi, PlayerPawn *pawn);

/* These functions specify equipment that you can only have one of; equipping
   a new one should unequip the old one. */
int UnequipWeapon (PlayerPawn *pawn);

int UnequipArmor (PlayerPawn *pawn);

int UnequipShield (PlayerPawn *pawn);

int UnequipHelmet (PlayerPawn *pawn);

int UnequipGauntlet (PlayerPawn *pawn);

int PawnItemCompatible (int class, int usage);

#endif /* HAVE_EQUIP_H */
