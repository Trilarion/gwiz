/*  levelup.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <stdlib.h>
#include "gwiz.h"
#include "text.h"
#include "menus.h"
#include "inn.h"
#include "uiloop.h"
#include "levelup.h"
#include "playerpawn.h"
#include "spells.h"
#include "spellmanage.h"

#ifndef RRAND
#define RRAND(min, max) (min+(int) (max*rand()/RAND_MAX+min))
#endif

extern GwizApp gwiz;
extern GwizInn inn;
const int levelthresh[] = {
    1100, /* fig */
    1500, /* mag */
    1400, /* cle */
    1200, /* thi */
    1800, /* wiz */
    2000, /* sam */
    1900, /* lor */
    2400  /* nin */
};

struct attrlimits_ {
    short str;
    short iq;
    short agi;
    short dev;
    short vit;
    short lck;
} attrlimits;

LevelUpEvent lev;

void HandleLevelUp (void)
{
    if (!CheckLvUpXP())
	return;
    MsgBox ("Level Up!");
    SetAttrLimits();
    inn.pawn->level++;
    lev.str = IncreaseStrength();
    lev.iq = IncreaseIq();
    lev.dev = IncreaseDevotion();
    lev.agi = IncreaseAgility();
    lev.vit = IncreaseVitality();


    lev.lck = IncreaseLuck();
    lev.spell = NULL;
			      
    switch (inn.pawn->class)
    {
    case FIGHTER:
	LvUpFig();
	break;
    case MAGE:
	LvUpMag();
	break;
    case CLERIC:
	LvUpCle();
	break;
    case THIEF:
	LvUpThi();
	break;
    case WIZARD:
	LvUpWiz();
	break;
    case SAMURAI:
	LvUpSam();
	break;
    case LORD:
	LvUpLor();
	break;
    case NINJA:
	LvUpNin();
	break;
    }

    EnforceMPRestrictions();

    lev.spell = NULL;
}

int CheckLvUpXP (void)
{
    Uint32 thresh = levelthresh[inn.pawn->class];
    Uint32 required = thresh;
    int i = 1;

    while (i < inn.pawn->level+1)
	{
	    required += thresh;
	    if (required > inn.pawn->counter.ep)
		return (FALSE);
	    thresh += thresh * 0.1;
	    if (required > 512000)
		return (CheckLvUpExtendedXP (required, i));
	    i++;
	}

    return (TRUE);
}

int CheckLvUpExtendedXP (Uint32 threshold, int level)
{
    int i;

    for (i = level; i < inn.pawn->level; i++)
	threshold += 512000;

    if (threshold < inn.pawn->counter.ep)
	return (TRUE);
    return (FALSE);
}

void LvUpFig (void)
{
    int modifier;

    if (rand() < 0.1)
	modifier = inn.pawn->attr.vit + inn.pawn->attr.luck;
    else
	modifier = inn.pawn->attr.vit;
    lev.hpinc = rand() % (modifier + 1) - 1;
}

void LvUpMag (void)
{
    int modifier;
    int spell = RRAND(1, 3);
    int i;

    if (rand() < 0.1)
	modifier = inn.pawn->attr.vit;
    else
	modifier = 5;
    lev.hpinc = RRAND(1, modifier);

    lev.mage_spell = TRUE;
    for (i = 6; i > 0; i--)
    {
	if (inn.pawn->mm.maxpoints[i])
	    inn.pawn->mm.maxpoints[i]++;
    }

    for (i = 0; i < 6; i++)
    {
	if (inn.pawn->mm.maxpoints[i+1]+spell > inn.pawn->mm.maxpoints[i])
	{
	    inn.pawn->mm.maxpoints[i] += spell;
	    LearnNewSpell();
	    return;
	}
    }
    LearnNewSpell();
    inn.pawn->mm.maxpoints[0] += spell;
}

void LvUpCle (void)
{
    int modifier;
    int spell = rand() % (3 + 1) - 1;
    int i;

    if (rand() < 0.1)
	modifier = inn.pawn->attr.vit;
    else
	modifier = 6;
    lev.hpinc = rand() % (modifier + 1) - 1;

    for (i = 6; i > 0; i--)
    {
	if (inn.pawn->cm.maxpoints[i])
	    inn.pawn->cm.maxpoints[i]++;
    }

    for (i = 0; i < 6; i++)
    {
	if (inn.pawn->cm.maxpoints[i+1]+spell > inn.pawn->cm.maxpoints[i])
	{
	    inn.pawn->cm.maxpoints[i] += spell;
	    return;
	}
    }
    inn.pawn->cm.maxpoints[0] += spell;
}

void LvUpThi (void)
{
    int modifier;

    if (rand() < 0.1)
	modifier = inn.pawn->attr.vit + 3;
    else
	modifier = 8;
    lev.hpinc = rand() % (modifier + 1) - 1;
}

void LvUpWiz (void)
{
    int modifier;
    int spell = (rand() < 0.5) ? 2 : 1;
    int i;

    if (rand() < 0.1)
	modifier = inn.pawn->attr.vit - 4;
    else
	modifier = 3;
    lev.hpinc = rand() % (modifier + 1) - 1;
    lev.mage_spell = (rand() < 0.5) ? FALSE : TRUE;

    if (lev.mage_spell)
    {
	for (i = 6; i > 0; i--)
	{
	    if (inn.pawn->mm.maxpoints[i])
		inn.pawn->mm.maxpoints[i]++;
	}
	
	for (i = 0; i < 6; i++)
	{
	    if (inn.pawn->mm.maxpoints[i+1]+spell > inn.pawn->mm.maxpoints[i])
	    {
		inn.pawn->mm.maxpoints[i] += spell;
		return;
	    }
	}
	inn.pawn->mm.maxpoints[0] += spell;
	LearnNewSpell ();
	return;
    }

    for (i = 6; i > 0; i--)
    {
	if (inn.pawn->cm.maxpoints[i])
	    inn.pawn->cm.maxpoints[i]++;
    }

    for (i = 0; i < 6; i++)
    {
	if (inn.pawn->cm.maxpoints[i+1]+spell > inn.pawn->cm.maxpoints[i])
	{
	    inn.pawn->cm.maxpoints[i] += spell;
	    return;
	}
    }
    inn.pawn->cm.maxpoints[0] += spell;
    LearnNewSpell ();
}

void LvUpSam (void)
{
    int modifier;
    int spell = rand() % (3 + 1) - 1;
    int i;

    /* This little ditty is because samurai typically get hp faster than
       fighters at first, but in the end, fighters usually have more anyway */
    if (inn.pawn->level < 50)
    {
	if (rand() < 0.1)
	    modifier = inn.pawn->attr.vit + inn.pawn->attr.luck + 10;
	else
	    modifier = inn.pawn->attr.vit + 10;
    } else {
	if (rand() < 0.1)
	    modifier = inn.pawn->attr.vit + inn.pawn->attr.luck/2;
	else
	    modifier = inn.pawn->attr.vit;
    }
    lev.hpinc = rand() % (modifier + 1) - 1;

    for (i = 6; i > 0; i--)
    {
	if (inn.pawn->mm.maxpoints[i])
	    inn.pawn->mm.maxpoints[i]++;
    }

    for (i = 0; i < 6; i++)
    {
	if (inn.pawn->mm.maxpoints[i+1]+spell > inn.pawn->mm.maxpoints[i])
	{
	    inn.pawn->mm.maxpoints[i] += spell;
	    return;
	}
    }
    inn.pawn->mm.maxpoints[0] += spell;
}

void LvUpLor (void)
{
    int spell = rand() % (3 + 1) - 1;
    int i;

    /* Lords are simple enough; they can't get more than vitality permits */
    lev.hpinc = rand() % (inn.pawn->attr.vit + 1) - 1;

    for (i = 6; i > 0; i--)
    {
	if (inn.pawn->cm.maxpoints[i])
	    inn.pawn->cm.maxpoints[i]++;
    }

    for (i = 0; i < 6; i++)
    {
	if (inn.pawn->cm.maxpoints[i+1]+spell > inn.pawn->cm.maxpoints[i])
	{
	    inn.pawn->cm.maxpoints[i] += spell;
	    return;
	}
    }
    inn.pawn->cm.maxpoints[0] += spell;
}

void LvUpNin (void)
{
    /* Equally simple.  ninjas typically do not have many HPs at all. */
    lev.hpinc = rand() % (inn.pawn->attr.vit/2 + 1) - 1;
}

int IncreaseStrength (void)
{
    if (attrlimits.str != inn.pawn->attr.str)
	{
	    return (CalculateAttrLuck());
	}
    return (FALSE);
}

int IncreaseIq (void)
{
    if (attrlimits.iq != inn.pawn->attr.iq)
	{
	    return (CalculateAttrLuck());
	}
    return (FALSE);
}

int IncreaseDevotion (void)
{
    if (attrlimits.dev != inn.pawn->attr.dev)
	{
	    return (CalculateAttrLuck());
	}
    return (FALSE);
}

int IncreaseVitality (void)
{
    if (attrlimits.vit != inn.pawn->attr.vit)
	{
	    return (CalculateAttrLuck());
	}
    return (FALSE);
}

int IncreaseAgility (void)
{
    if (attrlimits.agi != inn.pawn->attr.agi)
	{
	    return (CalculateAttrLuck());
	}
    return (FALSE);
}

int IncreaseLuck (void)
{
    if (attrlimits.lck != inn.pawn->attr.luck)
	{
	    return (CalculateAttrLuck());
	}
    return (FALSE);
}

void SetAttrLimits (void)
{
    switch (inn.pawn->race)
    {
    case HUMAN:
	attrlimits.str = HUMAN_STR;
	attrlimits.iq = HUMAN_IQ;
	attrlimits.dev = HUMAN_DEV;
	attrlimits.vit = HUMAN_VIT;
	attrlimits.agi = HUMAN_AGI;
	attrlimits.lck = HUMAN_LUCK;
	break;
    case ELF:
	attrlimits.str = ELF_STR;
	attrlimits.iq = ELF_IQ;
	attrlimits.dev = ELF_DEV;
	attrlimits.vit = ELF_VIT;
	attrlimits.agi = ELF_AGI;
	attrlimits.lck = ELF_LUCK;
	break;
    case DWARF:
	attrlimits.str = DWARF_STR;
	attrlimits.iq = DWARF_IQ;
	attrlimits.dev = DWARF_DEV;
	attrlimits.vit = DWARF_VIT;
	attrlimits.agi = DWARF_AGI;
	attrlimits.lck = DWARF_LUCK;
	break;
    case GNOME:
	attrlimits.str = GNOME_STR;
	attrlimits.iq = GNOME_IQ;
	attrlimits.dev = GNOME_DEV;
	attrlimits.vit = GNOME_VIT;
	attrlimits.agi = GNOME_AGI;
	attrlimits.lck = GNOME_LUCK;
	break;
    case HOBBIT:
	attrlimits.str = HOBBIT_STR;
	attrlimits.iq = HOBBIT_IQ;
	attrlimits.dev = HOBBIT_DEV;
	attrlimits.vit = HOBBIT_VIT;
	attrlimits.agi = HOBBIT_AGI;
	attrlimits.lck = HOBBIT_LUCK;
	break;
    }
}

int CalculateAttrLuck (void)
{
    if (RRAND(1, 100) < inn.pawn->attr.luck*0.75)
	return (TRUE);
    return (FALSE);
}

void EnforceMPRestrictions (void)
{
    int i;
    int lim = 63;

    for (i = 0; i < 7; i++)
    {
	if (inn.pawn->cm.maxpoints[i] > lim)
	    inn.pawn->cm.maxpoints[i] = lim;
	if (inn.pawn->mm.maxpoints[i] > lim)
	    inn.pawn->mm.maxpoints[i] = lim;
	lim -= 9;
    }
}

void LearnNewSpell (void)
{
    int registered = 0;
    int i;
    SpellNo first_spell = NO_SPELL;
    SpellNo last_spell = NO_SPELL;
    int all_spells = TRUE;
    if (lev.mage_spell)
    {
	for (i = 0; i < 7; i++)
	    if (!inn.pawn->mm.maxpoints[i])
		break;
	switch (i)
	{
	case 1:
	    first_spell = IGNIS;
	    last_spell = CLARITAS;
	    break;
	case 2:
	    first_spell = ALACRITAS;
	    last_spell = CORDA_PETRI;
	    break;
	case 3:
	    first_spell = APERIRE;
	    last_spell = DISRUPTUS;
	    break;
	case 4:
	    first_spell = PUGNUS;
	    last_spell = SENSU_PRIVARE;
	    break;
	case 5:
	    first_spell = ACCIRE;
	    last_spell = ARCUS_PLUVIUS;
	    break;
	case 6:
	    first_spell = PARIAE_VIS;
	    last_spell =  PROCELLA_GELI;
	    break;
	case 7:
	    first_spell = LOCUS_NOVUS;
	    last_spell = ARS_MAGICA_DEI;
	    break;
	}

	for (i = first_spell; i < last_spell+1; i++)
	    if (inn.pawn->spellsknown[i])
		first_spell = IGNIS;

    } else {
	for (i = 0; i < 7; i++)
	    if (!inn.pawn->cm.maxpoints[i])
		break;
	switch (i)
	{
	case 1:
	    first_spell = MEDERI;
	    last_spell = PARMA;
	    break;
	case 2:
	    first_spell = FASCINARE;
	    last_spell = CORPUS_REPERIRE;
	    break;
	case 3:
	    first_spell = AGNOSCERE;;
	    last_spell = MAGICE_SICCARE;
	    break;
	case 4:
	    first_spell = MEDERI_POTENTI;
	    last_spell = VENTI_NOVACULA;
	    break;
	case 5:
	    first_spell = MAGNUS_MEDERUM;
	    last_spell = INTERFICERE;
	    break;
	case 6:
	    first_spell = REVOCARE;
	    last_spell =  VENTI_IGNIS;
	    break;
	case 7:
	    first_spell = VENTI_PETRUS;
	    last_spell = INFERUS_EXCITARE;
	    break;
	}

	for (i = first_spell; i < last_spell+1; i++)
	    if (inn.pawn->spellsknown[i])
		first_spell = MEDERI;

    }
    for (i = first_spell; i < last_spell+1; i++)
	if (!inn.pawn->spellsknown[i])
	    all_spells = FALSE;
    if (all_spells)
	return;

    while (registered < 5)
    {
	for (i = first_spell; i < last_spell+1; i++)
	    if ((!inn.pawn->spellsknown[i]) && (registered < 5))
	    {
		lev.spell_candidates[registered] = i;
		registered++;
	    }
    }

    i = (int) (5.0*rand()/(RAND_MAX+1.0));
    lev.spell = GetSpellName (lev.spell_candidates[i]);
    inn.pawn->spellsknown[i] = TRUE;
}

