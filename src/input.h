#ifndef USING_INPUT_H
#define USING_INPUT_H

typedef struct GwizInputConf_ {
    SDL_Surface *window;

    int pos;
} GwizInputConf;

void InputConfig(void);

#endif /* USING_INPUT_H */
