/*  menus.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_MENUS_H
#define HAVE_MENUS_H
#ifndef UP
#define UP 0
#define DOWN 1
#endif /* UP */

typedef struct GwizMenu_ GwizMenu;
typedef struct MenuDesc_ MenuDesc;

struct MenuDesc_ {
    SDL_Surface *area;
    short pos;
    short maxpos;
    short cx;
    short cy;
};

struct GwizMenu_ {
    MenuDesc desc;
    SDL_Surface *oldcanvas;
    SDL_Surface *area;
    SDL_Rect cursorinfo;
    int widestentry;
    int height;
    int width;
    int x; /* so MenuLoop() can tell where to draw the menu */
    int y;
    int cursorinitpos;
};
    
/* Create a list of menu items out of **labels until labels[number] == NULL */
int NewGwizMenu(SDL_Surface *target, char **labels, int x, int y, int initpos);

void SetActualCoords (SDL_Surface *target, int *x, int *y, GwizMenu *menu);

int MenuLoop (SDL_Surface *target, GwizMenu *menu);

void MoveCursor (int direction, MenuDesc *desc);

#endif /* HAVE_MENUS_H */
