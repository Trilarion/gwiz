/*  encounter.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_ENCOUNTER_H
#define HAVE_ENCOUNTER_H

#define ENCOUNTER_BATTLE   0x00000001
#define ENCOUNTER_FRIENDLY 0x00000002
#define ENCOUNTER_NPC      0x00000004

typedef struct GwizBattle_ GwizBattle;
typedef struct MonsterAction_ MonsterAction;
typedef struct PawnAction_ PawnAction;

struct PawnAction_ {
    
};

struct MonsterAction_ {
    
};

struct GwizBattle_ {
    MonsterPawn *mon[4][9]; /* Up to nine pawns in four parties */
    MonsterAction mact[4][9];
    PawnAction pact[6];
};

typedef enum {
    NO_NPC
} NpcType;

void CheckEncounter (void);

void Encounter (int npc);

void EncounterMonsters (void);

#endif /* HAVE_ENCOUNTER_H */
