/*  text.h: (c) 2002 sibn

    This file is part of Gwiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_TEXT_H
#define HAVE_TEXT_H

#define TEXTMSG_TOP 0x00000001
#define TEXTMSG_BTM 0x00000002

#define BORDERPAD 1
#define BORDERWIDTH (gwiz.tbord[0]->w + BORDERPAD)
#define BORDERHEIGHT (gwiz.tbord[0]->h + BORDERPAD)

/* Set up game for use with fonts.  Sets gwiz.font.<member> settings to the
   appropriate values for use. */
void InitGwizFonts (void);

/* Returns an SDL Surface big enough to hold text of "width" and "height" */
SDL_Surface *NewTextBox (int width, int height);

/* Render msg as an SDL_Surface, and blit it to box.  Wrap it if necessary */
SDL_Surface *NewTextMsg (char *msg);
SDL_Surface *NewVertTextMsg (char **msg, int align);

/* Load pixmaps used in making the border. */
void LoadBorders (void);

/* suitable for extremely short messages */
int MsgBox (char *msg);

/* Render numbers for 0-25 */
void InitCommonNums(void);

/* Render graphics for class names */
void InitClassPix (void);

/* grab keyboard input with char *title, for N bytes of input */
void NewTextEntry (char *title, char *inputmsg, int inputlength);

SDLKey ShiftNumberRow (SDLKey key);

SDL_Surface *GwizRenderText(char *msg);

#endif /* HAVE_TEXT_H */
