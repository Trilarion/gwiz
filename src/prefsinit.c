/*  prefsdat.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdlib.h>
#include "glib.h"
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "SDL_thread.h"
#include "gwiz.h"
#include "text.h"
#include "uiloop.h"
#include "playerpawn.h"
#include "prefsdat.h"

extern GwizApp gwiz;

void InitPrefs (int argc, char **argv)
{
    gint i;

    gwiz.cond.lumen = 0;
    gwiz.cond.volitare = 0;
    gwiz.cond.agnoscere = 0;

    gwiz.font.ptsize = 16;
    gwiz.font.aa = TRUE;
    gwiz.udata.home = getenv ("HOME");
    gwiz.udata.gwiz = MakePath (gwiz.udata.home, "/.gwiz");
    gwiz.udata.cfg = MakePath (gwiz.udata.gwiz, "/cfg09");
    gwiz.udata.party = MakePath (gwiz.udata.gwiz, "/party");
    gwiz.udata.log = MakePath (gwiz.udata.gwiz, "/log");
    gwiz.udata.monsters = PKGDATADIR "/monsters";
    gwiz.udata.items = PKGDATADIR "/items";

    gwiz.key.quit = 0;
    gwiz.key.fwd = SDLK_KP8;
    gwiz.key.bkwd = SDLK_KP5;
    gwiz.key.lft = SDLK_KP7;
    gwiz.key.rgt = SDLK_KP9;
    gwiz.key.slft = SDLK_KP4;
    gwiz.key.srgt = SDLK_KP6;
    gwiz.key.fta = SDLK_KP2;
    gwiz.key.act = SDLK_KP_ENTER;
    gwiz.key.cancel = SDLK_KP0;

    gwiz.joy.enabled = TRUE;
    gwiz.joy.whichdev = 0;
    gwiz.joy.fwd = -16384;
    gwiz.joy.lft = -16384;
    gwiz.joy.rgt = 16384;
    gwiz.joy.fta = 16384;
    gwiz.joy.bkwd = -1;
    gwiz.joy.slft = -1;
    gwiz.joy.srgt = -1;
    gwiz.joy.act = 0;
    gwiz.joy.cancel = 1;

    gwiz.vidmode = SDL_HWSURFACE|SDL_DOUBLEBUF;
#if SDL_BYTE_ORDER == SDL_BIGENDIAN
    gwiz.rmask = 0xff000000;
    gwiz.gmask = 0x00ff0000;
    gwiz.bmask = 0x0000ff00;
#else
    gwiz.rmask = 0x000000ff;
    gwiz.gmask = 0x0000ff00;
    gwiz.bmask = 0x00ff0000;
#endif
    gwiz.nmask = 0x00000000;
    gwiz.bgc = 0x00000000;
    gwiz.fgc = 0x00000000;
    gwiz.bpp = 32;
    gwiz.logging = TRUE;
    gwiz.freshlog = 0;
    gwiz.ffi = TRUE;
    gwiz.zaa = TRUE;

    for (i = 0; i < 6; i++)
	gwiz.pawnno[i] = -1;

}

void LoadPrefs (void)
{

}

void SavePrefs (void)
{

}

void ShutdownPrefs (void)
{

}

