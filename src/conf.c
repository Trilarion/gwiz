#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include "gwiz.h"
#include "keynames.h"
#include "conf.h"

#define MAXLINELENGTH 128

extern GwizApp gwiz;

void
ConfCreate (char *in_name, char *out_name)
{
    FILE *INPUT, *OUTPUT;
    char buf[256];
    int read_bytes;

    INPUT = fopen (in_name, "r");
    OUTPUT = fopen (out_name, "w");
    if ((INPUT == NULL) || (OUTPUT == NULL))
	GErr ("problem opening input file for reading and/or output file for writing: \nin: %s | out: %s\n", in_name, out_name);
    
    while (1)
	{
	    read_bytes = fread (buf, 1, 256, INPUT);
	    fwrite (buf, read_bytes, 1, OUTPUT);
	    if (read_bytes != 256)
		break;
	}
    fclose (INPUT);
    fclose (OUTPUT);

    if (!ConfExists(out_name))
	GErr ("unable to create config file: %s", out_name);
}

int
ConfExists (char *fname)
{
    struct stat stbuf;
    int status;

    status = stat (fname, &stbuf);
    if (status != 0)
	{
	    if (errno == ENOENT)
		fprintf (stderr, "requested file %s does not exist\n", fname);
	    else
		fprintf (stderr, "unspecified error opening file: %s\n", 
			 fname);
	    return (0);
	}
    return (1);
}

void
OpenConf (char *fname)
{
    FILE *fd;
    char lastline[MAXLINELENGTH];

    printf ("%s\n", PKGDATADIR);
    if (!ConfExists (fname))
	ConfCreate (PKGDATADIR "/gwiz.ini", fname);

    fd = fopen (fname, "r");
    if (fd == NULL)
	{
	    fprintf (stderr, "unable to open ini file for reading\n");
	    return;
	}
    if (fgets (lastline, MAXLINELENGTH, fd) == NULL)
	GErr ("unable to read from config file: %s", fname);
    if (strcasecmp (lastline, "conf_version=1\n") != 0)
	{
	    fprintf (stderr, "conf file appears to be invalid.  attempting to recreate: %s", fname);
	    fclose (fd);
	    ConfCreate (PKGDATADIR "/gwiz.ini", fname);
	    OpenConf (fname); /* reopen conf file.  FIXME infinite loop? */
	}
    while (fgets(lastline, MAXLINELENGTH, fd) != NULL)
	{
	    int i = 0;
	    while (i < MAXLINELENGTH)
		{
		    if (i == MAXLINELENGTH-1)
			GErr ("unable to read %s:%d: maximum line length in config file is %d", fname, i, MAXLINELENGTH);
		    if (lastline[i] == '\n')
			{
			    lastline[i] = '\0';
			    break;
			}
		    i++;
		}
	    ParseConfOpt (lastline);
	}
}

#define STREQ(string, strang, num) ((strncasecmp(string, strang, num)==0)?1:0) 

int GBool (char *string)
{
    if ((strcasecmp(string, "true") == 0) ||
	(strcasecmp(string, "t"   ) == 0) ||
	(strcasecmp(string, "yes" ) == 0) ||
	(strcasecmp(string, "y"   ) == 0) ||
	(strcasecmp(string, "1"   ) == 0))
	return 1;
    return 0;
}

void ParseConfOpt (char *line)
{
    int i;
    for (i = 0; i < MAXLINELENGTH; i++)
	if (line[i-1] == '=')
	    break; /* line[i] now points to what i'm after */
    /* FIXME add some sanity checks to this */

    /* vid_ section */
    if (STREQ("vid_fullscreen=", line, i))
	if (GBool (&line[i])) gwiz.vidmode |= SDL_FULLSCREEN;
    if (STREQ("vid_hwrender=", line, i))
	if (GBool (&line[i])) gwiz.vidmode |= SDL_HWSURFACE;
    if (STREQ("vid_depth=", line, i))
	gwiz.bpp = atoi (&line[i]);
    if (STREQ("vid_doublebuf=", line, i))
	if (GBool (&line[i])) gwiz.vidmode |= SDL_DOUBLEBUF;
    if (STREQ("vid_zoom_aa=", line, i))
	gwiz.zaa = atoi (&line[i]);
    
    /* font_ section */
    if (STREQ("font_size=", line, i))
	gwiz.font.ptsize = atoi (&line[i]);
    if (STREQ("font_aa=", line, i))
	 gwiz.font.aa = GBool(&line[i]);

    /* keys section */
    if (STREQ("key_quit=", line, i))
	gwiz.key.quit = CharToKey (&line[i], 0);
    if (STREQ("key_ok=", line, i))
	gwiz.key.act = CharToKey (&line[i], SDLK_KP_ENTER);
    if (STREQ("key_cancel=", line, i))
	gwiz.key.cancel = CharToKey (&line[i], SDLK_KP0);
    if (STREQ("key_forward=", line, i))
	gwiz.key.fwd = CharToKey (&line[i], SDLK_KP8);
    if (STREQ("key_backward=", line, i))
	gwiz.key.bkwd = CharToKey (&line[i], SDLK_KP5);
    if (STREQ("key_left=", line, i))
	gwiz.key.lft = CharToKey (&line[i], SDLK_KP7);
    if (STREQ("key_right=", line, i))
	gwiz.key.rgt = CharToKey (&line[i], SDLK_KP9);
    if (STREQ("key_aboutface=", line, i))
	gwiz.key.fta = CharToKey (&line[i], SDLK_KP2);
    if (STREQ("key_stepleft=", line, i))
	gwiz.key.slft = CharToKey (&line[i], SDLK_KP4);
    if (STREQ("key_stepright=", line, i))
	gwiz.key.srgt = CharToKey (&line[i], SDLK_KP6);

    /* not sure why i don't have the joystick section here.  FIXME at 11. */
}

void SaveConf (char *fname)
{
    FILE *fd;

    if ((fd = fopen (fname, "w")) == NULL)
	{
	    fprintf (stderr, "unable to open config file for writing: %s\n",
		     fname);
	    return;
	}

    fprintf (fd, "conf_version=1\n");

    /* video section */
    fprintf (fd, "vid_fullscreen=%s\n", gwiz.vidmode&SDL_FULLSCREEN ? "true" : "false");
    fprintf (fd, "vid_hwrender=%s\n", gwiz.vidmode&SDL_HWSURFACE ? "true" : "false");
    fprintf (fd, "vid_depth=%d\n", gwiz.bpp);
    fprintf (fd, "vid_doublebuf=%s\n", gwiz.vidmode&SDL_DOUBLEBUF ? "true" : "false");
    fprintf (fd, "vid_zoom_aa=%s\n", gwiz.zaa ? "true" : "false");

    /* font section */
    fprintf (fd, "font_size=%d\n", gwiz.font.ptsize);
    fprintf (fd, "font_aa=%s\n", gwiz.font.aa ? "true" : "false");

    /* keys section */
    fprintf (fd, "key_quit=%s\n",      KeyToChar (gwiz.key.quit));
    fprintf (fd, "key_ok%s=\n",        KeyToChar (gwiz.key.act));
    fprintf (fd, "key_cancel=%s\n",    KeyToChar (gwiz.key.cancel));
    fprintf (fd, "key_forward=%s\n",   KeyToChar (gwiz.key.fwd));
    fprintf (fd, "key_backward=%s\n",  KeyToChar (gwiz.key.bkwd));
    fprintf (fd, "key_left=%s\n",      KeyToChar (gwiz.key.lft));
    fprintf (fd, "key_right=%s\n",     KeyToChar (gwiz.key.rgt));
    fprintf (fd, "key_aboutface=%s\n", KeyToChar (gwiz.key.fta));
    fprintf (fd, "key_stepleft=%s\n",  KeyToChar (gwiz.key.slft));
    fprintf (fd, "key_stepright=%s\n", KeyToChar (gwiz.key.srgt));

    /* this seems to be handled elsewhere */
    /* joystick section 
    fprintf (fd, "joy_devicenumber=%d\n", gwiz.joy.whichdev);
    fprintf (fd, "joy_forward=%d\n",      gwiz.joy.fwd);
    fprintf (fd, "joy_backward=%d\n",     gwiz.joy.bkwd);
    fprintf (fd, "joy_left=%d\n",         gwiz.joy.lft);
    fprintf (fd, "joy_right=%d\n",        gwiz.joy.rgt);
    fprintf (fd, "joy_aboutface=%d\n",    gwiz.joy.fta);
    fprintf (fd, "joy_stepleft=%d\n",     gwiz.joy.slft);
    fprintf (fd, "joy_stepright=%d\n",    gwiz.joy.srgt);
    fprintf (fd, "joy_ok=%d\n",           gwiz.joy.act);
    fprintf (fd, "joy_cancel=%d\n",       gwiz.joy.cancel);
    fclose (fd);
    */
}
