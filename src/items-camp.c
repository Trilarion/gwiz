/*  items-camp.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <strings.h>
#include "gwiz.h"
#include "items.h"
#include "spellmanage.h"
#include "party.h"

extern GwizApp gwiz;

void UseItemCamp (Inventory *item, PlayerPawn *origin, void *target)
{
    GwizSpell *spell;
    int itemeffect = (item->magice > item->clerice) ? item->clerice :
	item->magice;
    int whichpawn;
    if (itemeffect == NO_SPELL)
	return;
    spell = NewSpell (itemeffect);
    if (spell->targettype == T_PAWN)
	{
	    whichpawn = NewMiniPawnList("Use item on whom?");
	    if (whichpawn < 0)
		return;
	    UseItemCampPawn (item, origin, &gwiz.pawn[whichpawn], spell);
	}
    if (spell->targettype == T_PARTY)
	{
	    UseItemCampParty (item, origin, spell);
	}
    if (spell->targettype == T_NONE)
	{
	    UseItemCampNone (item, spell);
	}
    DestroyItemOnUse (origin, item);

    DestroySpell (&spell);
}

void UseItemCampPawn (Inventory *item, PlayerPawn *origin, PlayerPawn *target,
		      GwizSpell *spell)
{

}

void UseItemCampParty (Inventory *item, PlayerPawn *origin, GwizSpell *spell)
{

}

void UseItemCampNone (Inventory *item, GwizSpell *spell)
{

}
