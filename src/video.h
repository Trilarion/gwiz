/*  video.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_VIDEO_H
#define HAVE_VIDEO_H

/* Figures out where the walls are */
void MapWalls(void);

/* For when there's a lumen spell in effect */
void MapLightWalls(void);

/* Draws a wall, dependent on the position */
void RenderObject (int obj, int pos);

/* Draw the walls that a lumen spell will affect */
void RenderLightObject (int obj, int pos);

#endif /* HAVE_VIDEO_H */



