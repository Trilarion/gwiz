/*  castle.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "gwiz.h"
#include "uiloop.h"
#include "text.h"
#include "maploader.h"
#include "menus.h"
#include "playerpawn.h"
#include "castle.h"
#include "playergen.h"
#include "party.h"
#include "inspect.h"
#include "items.h"
#include "shop.h"
#include "inn.h"
#include "multiline.h"

extern GwizApp gwiz;
extern GwizShopWindow gsw;
extern SDL_Color fg;
extern SDL_Color bg;

char *mazeinstructions = "You cannot enter the maze with no characters in your party.  First go to the Training Grounds and create at least one character.  Return to the castle, go to the Ranger\'s Respite Tavern, and add at least one character to your party.";

void EnterCastle (void)
{
    SDL_Rect dest;
    int select = 0;
    char *mainmenu[] = {
	"Ranger\'s Respite Tavern",
	"Elysian Hostel",
	"The Trader\'s Freemarket",
	"(D) Temple of Life",
	"Edge of Town",
	NULL
    };

    /* Useful for managing player data; where pawns save etc. */
    gwiz.x = 128;
    gwiz.y = 128;
    gwiz.z = 0;

    dest.x = 0;
    dest.y = 0;
    dest.h = gwiz.canvas->h;
    dest.w = gwiz.canvas->w;
    SDL_FillRect (gwiz.canvas, &dest, 0);
    SDL_Flip (gwiz.canvas);
    UnloadWalls();
    UnloadDoors();
    while ((select = NewGwizMenu (gwiz.canvas, mainmenu, -1, 32, select)) != -2)
	{
	    switch (select)
	        {
		case -1:
		    EnterEdge(&select);
		    break;
		case 0:
		    EnterTavern();
		    break;
		case 1:
		    if (gwiz.pawnno[0] != -1)
			EnterInn();
		    break;
		case 2:
		    if (gwiz.pawnno[0] != -1)
			EnterTrade();
		    break;
		case 3:
		    if (gwiz.pawnno[0] != -1)
			EnterTemple();
		    break;
		case 4:
		    EnterEdge(&select);
		    break;
		}
	    SDL_FillRect (gwiz.canvas, &dest, 0);
	    SDL_Flip (gwiz.canvas);
	    if (select == -2)
		{
		    LoadWalls();
		    LoadDoors();
		    break;
		}
	}
}

void EnterTavern (void)
{
    char *cafemenu[] = {
	"Add Member",
	"Remove Member",
	"Inspect",
	"Divvy Gold",
	"Leave",
	NULL
    };
    int yoffset = 30+gwiz.tbord[0]->h;
    int option = 0;
    while ((option = NewGwizMenu (gwiz.canvas, cafemenu, -1, yoffset,
				  option)) != -1)
	switch (option)
	    {
	    case -1:
		return;
	    case 0:
		AddPawnToParty();
		break;
	    case 1:
		RemovePawnFromParty();
		break;
	    case 2:
		InspectPawn (0);
		break;
	    case 3:
		DivvyGold();
		break;
	    case 4:
		return;
	    }
}

void EnterTrade (void)
{
    int option = 0;
    char *bazaarmenu[] = {
	"Buy",
	"Sell",
	"(D) Uncurse",
	"(D) Identify",
	"Pool Gold",
	"Leave",
	NULL
    };
    int shoppingpawn;

    shoppingpawn = NewMiniPawnList ("Who will enter?");
    if (shoppingpawn < 0)
	return;
    gsw.pawn = &gwiz.pawn[shoppingpawn];
    if (gsw.pawn < 0)
	return; /* no pawns here */
    while((option = NewGwizMenu (gwiz.canvas, bazaarmenu, -1,
				 30+gwiz.tbord[0]->h, option)) != -1)
	switch (option)
	    {
	    case 0:
		GetTradeMode();
		break;
	    case 1:
		Sell (option);
		break;
	    case 2:
		break;
	    case 3:
		break;
	    case 4:
		PoolGold(NewMiniPawnList("Who will receive the money?"));
		break;
	    case 5:
		return;
	    }
}

void GetTradeMode (void)
{
    int option = 0;
    char *goods[] = {
	"Weapon",
	"Armor",
	"Shield",
	"Helmet",
	"Gauntlet",
	"Misc",
	"Scroll",
	"Leave",
	NULL
    };
    while ((option = NewGwizMenu (gwiz.canvas, goods, -1, -1, option)) != -1)
	{
	    if (option == 7)
		break;
	    Buy (option);
	}
}

void EnterInn (void)
{
    int whichpawn;
    while ((whichpawn = NewMiniPawnList("Who will enter?")) != -1)
	BeginStay(whichpawn);
}

void EnterTemple (void)
{
    
}

void EnterEdge (int *entermaze)
{
    int select = 0;
    char *edgemenu[] = {
	"Enter the Maze",
	"Training Grounds",
	"(D) Restart Adventuring Party",
	"Leave Game",
	"Castle",
	NULL
    };
    while ((select = NewGwizMenu(gwiz.canvas, edgemenu, -1, 
				 30+gwiz.tbord[0]->h, select)) != -2)
	{
	    switch (select)
		{
		case -1:
		    return;
		case 0:
		    if (CountPartyMembers() > -1)
			{
			    gwiz.z = 1;
			    *entermaze = -2;
			}
		    else
			NewMltm (FALSE, mazeinstructions);
		    return;
		case 1:
		    EnterTraining ();
		    break;
		case 2:
		    break;
		case 3:
		    VerifyQuit();
		    break;
		case 4:
		    return;
		}
	    if (select == -2)
		return;
	}
}

void EnterTraining (void)
{
    int option = 0;
    char *trainmenu[] = {
	"Create",
	"Inspect",
	"Delete",
	"Change Name",
	"Change Class",
	"Reorder",
	"Leave",
	NULL
    };
    while ((option = NewGwizMenu(gwiz.canvas, trainmenu, -1,
				 gwiz.tbord[0]->h*2+30, option)) != -1)
	{
	    switch (option)
		{
		case 0:
		    GeneratePlayer();
		    break;
		case 1:
		    EmptyParty();
		    InspectPawn(1);
		    break;
		case 2:
		    EmptyParty();
		    DeletePawn();
		    break;
		case 3:
		    EmptyParty();
		    RenamePawn();
		    break;
		case 4:
		    EmptyParty();
		    ChangeClass();
		    break;
		case 5:
		    EmptyParty();
		    ReorderPartyFile();
		    break;
		case 6:
		    return;
		}
	}
}



