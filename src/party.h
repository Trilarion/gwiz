/*  playerpawn.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_PARTY_H
#define HAVE_PARTY_H

typedef struct GwizMiniPawnList_ GwizMiniPawnList;

struct GwizMiniPawnList_ {
    SDL_Surface *area;
    SDL_Rect cdest;
    int position;
    int maxposition;
    int requiredwidth; /* track the required width dynamically */
};

void AddPawnToParty (void);

void RemovePawnFromParty (void);

void EmptyParty (void);

/* these are here instead of playerpawn.c because they have very specifically
   party-oriented stuff */
int NewMiniPawnList (char *prompt);

void MoveMiniPawnListCursor (GwizMiniPawnList *gmpl, int direction);

void LoadMiniPawnListPix (GwizMiniPawnList *gmpl);

SDL_Surface *RenderMiniPawnListNames (GwizMiniPawnList *gmpl);

SDL_Surface *RenderMiniPawnListSexes (GwizMiniPawnList *gmpl);

SDL_Surface *RenderMiniPawnListAlis (GwizMiniPawnList *gmpl);

SDL_Surface *RenderMiniPawnListClasses (GwizMiniPawnList *gmpl);

SDL_Surface *RenderMiniPawnListHps (GwizMiniPawnList *gmpl);

SDL_Surface *RenderMiniPawnListHpMaxes (GwizMiniPawnList *gmpl);

int MiniPawnListLoop (GwizMiniPawnList *gmpl);

int GetLiteralPartyPawnNo (int which);

int CountPartyMembers (void);

void CompactPartyPawns (void);

void ReorderParty (void);

#endif /* HAVE_PARTY_H */
