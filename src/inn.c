/*  inn.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.#include <glib.h>


    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <stdlib.h>
#include "gwiz.h"
#include "text.h"
#include "menus.h"
#include "inn.h"
#include "uiloop.h"
#include "levelup.h"

extern GwizApp gwiz;
GwizInn inn;

void BeginStay (int whichpawn)
{
    char *beds[] = {
	"(free!)/Week The stables",
	"50  GP/Week  Economy suite",
	"200 GP/Week  Luxury chambers",
	"500 GP/Week  Royal suite",
	NULL
    };

    inn.pawn = &gwiz.pawn[whichpawn];
    inn.bed = 0;
    inn.cost = 0;
    inn.hpinc = 0;
    while (inn.bed != -1)
    {
	inn.bed = NewGwizMenu (gwiz.canvas, beds, -1, -1, inn.bed);
	switch (inn.bed)
	    {
	    case 0:
		inn.cost = 0;
		inn.hpinc = 0;
		break;
	    case 1:
		inn.cost = 50;
		inn.hpinc = 1;
		break;
	    case 2:
		inn.cost = 200;
		inn.hpinc = 5;
		break;
	    case 3:
		inn.cost = 500;
		inn.hpinc = 20;
		break;
	    case -1:
		return;
	    }

	HandleLevelUp();
	InnRecover();
    }
}

void InnRecover (void)
{
    int i = 0;

    if (inn.pawn->counter.gp < inn.cost)
	{
	    MsgBox ("You cannot afford that");
	    return;
	}
    inn.pawn->counter.hp += inn.hpinc;
    if (inn.pawn->counter.hp > inn.pawn->counter.hp_max)
	inn.pawn->counter.hp = inn.pawn->counter.hp_max;
    inn.pawn->counter.gp -= inn.cost;

    for (i = 0; i < 7; i++)
    {
	inn.pawn->mm.spellpoints[i] = inn.pawn->mm.maxpoints[i];
	inn.pawn->cm.spellpoints[i] = inn.pawn->cm.maxpoints[i];
    }
}
