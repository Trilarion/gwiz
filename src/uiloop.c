/*  uiloop.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <stdlib.h>
#include "gwiz.h"
#include "uiloop.h"
#include "prefsdat.h"
#include "text.h"
#include "maploader.h"
#include "menus.h"
#include "camp.h"
#include "keyconfig.h"
#include "playerpawn.h"
#include "castle.h"
#include "playergen.h"
#include "keynames.h"
#include "joystick.h"
#include "encounter.h"
#include "video.h"

extern GwizApp gwiz;

void UILoop (void)
{
    SDL_Event event;
    char redraw = TRUE;
    short x, y;
    
    gwiz.x = 128; /* Ground zero. (heh.  not any more. now it's 128. */
    gwiz.y = 128;
    gwiz.z = 1;
    gwiz.face = NORTH;
    
    EnterCastle();
    MapWalls();
    
    while (SDL_WaitEvent (&event) != 0)
	{
	    SDL_Event *e = &event;

	    x = gwiz.x;
	    y = gwiz.y;

	    if (EventIsCancel (e))
		{
		    Camp();
		}
	    if (EventIsUp (e))
		{
		    redraw = MovePlayer (FWD);
		}
	    if (EventIsStepBack (e))
		{
		    redraw = MovePlayer (BKWD);
		}
	    if (EventIsStepLeft (e))
		{
		    redraw = SlidePlayer (0);
		}
	    if (EventIsStepRight (e))
		{
		    redraw = SlidePlayer (1);
		}
	    if (EventIsLeft (e))
		{
		    SetFace (0);
		    redraw = TRUE;
		}
	    if (EventIsRight (e))
		{
		    SetFace (1);
		    redraw = TRUE;
		}
	    if (EventIsFta (e))
		{
		    SetFace (1);
		    MapWalls();
		    SDL_Flip (gwiz.canvas);
		    SDL_Delay(100); /* watch yourself turn :) */
		    SetFace (1);
		    MapWalls();
		}
	    if (EventIsOk (e))
		{
		    redraw = Act();
		}
	    if (event.key.keysym.sym == SDLK_F12)
		{
		    KeyConfig ();
		}
	    if (event.type == SDL_QUIT)
		VerifyQuit();

	    if (redraw)
		{
		    MapWalls();
		    CheckEncounter();
		    redraw = FALSE;
		}
	    if ((gwiz.x != x) || (gwiz.y != y))
		{
		    OnTileChanged();
		}
	}
}

char CheckMapObject (int type, short pos, short row, short col)
{
    char exists = FALSE;
    /* This will prevent the mapping of hidden doors. */
    if ((type >= NHDOOR) && (type <= WHDOOR))
	return exists;
    switch (gwiz.face)
	{
	case NORTH:
	    if (pos == OBJ_LEFT)
		type = (type == DOOR) ? WDOOR : WWALL;
	    if (pos == OBJ_FRONT)
		type = (type == DOOR) ? NDOOR : NWALL;
	    if (pos == OBJ_RIGHT)
		type = (type == DOOR) ? EDOOR : EWALL;
	    if (type & gwiz.map.tile[gwiz.x+col][gwiz.y-row])
		exists = TRUE;
	    break;
	case EAST:
	    if (pos == OBJ_LEFT)
		type = (type == DOOR) ? NDOOR : NWALL;
	    if (pos == OBJ_FRONT)
		type = (type == DOOR) ? EDOOR : EWALL;
	    if (pos == OBJ_RIGHT)
		type = (type == DOOR) ? SDOOR : SWALL;
	    if (type & gwiz.map.tile[gwiz.x+row][gwiz.y+col])
		exists = TRUE;
	    break;
	case SOUTH:
	    if (pos == OBJ_LEFT)
		type = (type == DOOR) ? EDOOR : EWALL;
	    if (pos == OBJ_FRONT)
		type = (type == DOOR) ? SDOOR : SWALL;
	    if (pos == OBJ_RIGHT)
		type = (type == DOOR) ? WDOOR : WWALL;
	    if (type & gwiz.map.tile[gwiz.x-col][gwiz.y+row])
		exists = TRUE;
	    break;
	case WEST:
	    if (pos == OBJ_LEFT)
		type = (type == DOOR) ? SDOOR : SWALL;
	    if (pos == OBJ_FRONT)
		type = (type == DOOR) ? WDOOR : WWALL;
	    if (pos == OBJ_RIGHT)
		type = (type == DOOR) ? NDOOR : NWALL;
	    if (type & gwiz.map.tile[gwiz.x-row][gwiz.y-col])
		exists = TRUE;
	    break;
	}
    return exists;
}

void SetFace (int turn)
{
    /* i didn't #define LEFT and RIGHT.  But they're 0 and 1. :) */
    switch (turn)
	{
	case 0:
	    if (gwiz.face == NORTH)
		gwiz.face = WEST;
	    else
		gwiz.face--;
	    break;
	case 1:
	    if (gwiz.face == WEST)
		gwiz.face = NORTH;
	    else
		gwiz.face++;
	    break;
	}
}

char MovePlayer (int direction)
{
    char moved = FALSE;
    switch (gwiz.face)
	{
	case NORTH:
	    if ((direction == FWD) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & NWALL) != NWALL))
		{
		    gwiz.y--;
		    moved = TRUE;
		}
	    else if ((direction == FWD) &&
		     (gwiz.map.tile[gwiz.x][gwiz.y] & NWALL ) == NWALL)
		MsgBox("Ouch!");
	    if ((direction == BKWD) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & SWALL) != SWALL))
		{
		    gwiz.y++;
		    moved = TRUE;
		}
	    else if ((direction == BKWD) &&
		     (gwiz.map.tile[gwiz.x][gwiz.y] & SWALL) == SWALL)
		MsgBox("Ouch!");
	    break;
	case EAST:
	    if ((direction == FWD) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & EWALL) != EWALL))
		{
		    gwiz.x++;
		    moved = TRUE;
		}
	    else if ((direction == FWD) &&
		     (gwiz.map.tile[gwiz.x][gwiz.y] & EWALL) == EWALL)
		MsgBox("Ouch!");
	    if ((direction == BKWD) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & WWALL) != WWALL))
		{
		    gwiz.x--;
		    moved = TRUE;
		}
	    else if ((direction == BKWD) &&
		     (gwiz.map.tile[gwiz.x][gwiz.y] & WWALL) == WWALL)
		MsgBox("Ouch!");
	    break;
	case SOUTH:
	    if ((direction == FWD) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & SWALL) != SWALL))
		{
		    gwiz.y++;
		    moved = TRUE;
		}
	    else if ((direction == FWD) &&
		     (gwiz.map.tile[gwiz.x][gwiz.y] & SWALL) == SWALL)
		MsgBox("Ouch!");
	    if ((direction == BKWD) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & NWALL) != NWALL))
		{
		    gwiz.y--;
		    moved = TRUE;
		}
	    else if ((direction == BKWD) &&
		     (gwiz.map.tile[gwiz.x][gwiz.y] & NWALL) == NWALL)
		MsgBox("Ouch!");
	    break;
	case WEST:
	    if ((direction == FWD) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & WWALL) != WWALL))
		{
		    gwiz.x--;
		    moved = TRUE;
		}
	    else if ((direction == FWD) &&
		     ((gwiz.map.tile[gwiz.x][gwiz.y] & WWALL) == WWALL))
		MsgBox("Ouch!");
	    if ((direction == BKWD) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & EWALL) != EWALL))
		{
		    gwiz.x++;
		    moved = TRUE;
		}
	    else if ((direction == BKWD) &&
		     ((gwiz.map.tile[gwiz.x][gwiz.y] & EWALL) == EWALL))
		MsgBox("Ouch!");
	    break;
	}
    return (moved);
}

char SlidePlayer (int direction)
{
    char moved = FALSE;
    /* Note I could easily have used a "SetFace(0);MovePlayer(FWD);
       SetFace(1) combination to do this work, but it would probably be
       obvious that the direction were changing on a lot of machines. */
    switch (gwiz.face)
        {
	case NORTH:
	    if ((direction == 0) && 
		((gwiz.map.tile[gwiz.x][gwiz.y] & WWALL) != WWALL))
		{
		    gwiz.x--;
		    moved = TRUE;
		}
	    else if ((direction == 0) &&
		     (gwiz.map.tile[gwiz.x][gwiz.y] & WWALL) == WWALL)
		MsgBox("Ouch!");
	    if ((direction == 1) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & EWALL) != EWALL))
		{
		    gwiz.x++;
		    moved = TRUE;
		}
	    else if ((direction == 1) &&
		     (gwiz.map.tile[gwiz.x][gwiz.y] & EWALL) == EWALL)
		MsgBox("Ouch!");
	    break;
	case EAST:
	    if ((direction == 0) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & NWALL) != NWALL))
		{
		    gwiz.y--;
		    moved = TRUE;
		}
	    else if ((direction == 0) &&
		     ((gwiz.map.tile[gwiz.x][gwiz.y] & NWALL) == NWALL))
		MsgBox("Ouch!");
	    if ((direction == 1) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & SWALL) != SWALL))
		{
		    gwiz.y++;
		    moved = TRUE;
		}
	    else if ((direction == 1) &&
		     ((gwiz.map.tile[gwiz.x][gwiz.y] & SWALL) == SWALL))
		MsgBox("Ouch!");
	    break;
	case SOUTH:
	    if ((direction == 0) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & EWALL) != EWALL))
		{
		    gwiz.x++;
		    moved = TRUE;
		}
	    else if ((direction == 0) &&
		     ((gwiz.map.tile[gwiz.x][gwiz.y] & EWALL) == EWALL))
		MsgBox("Ouch!");
	    if ((direction == 1) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & WWALL) != WWALL))
		{
		    gwiz.x--;
		    moved = TRUE;
		}
	    else if ((direction == 1) &&
		     ((gwiz.map.tile[gwiz.x][gwiz.y] & WWALL) == WWALL))
		MsgBox("Ouch!");
	    break;
	case WEST:
	    if ((direction == 0) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & SWALL) != SWALL))
		{
		    gwiz.y++;
		    moved = TRUE;
		}
	    else if ((direction == 0) &&
		     ((gwiz.map.tile[gwiz.x][gwiz.y] & SWALL) == SWALL))
		MsgBox("Ouch!");
	    if ((direction == 1) &&
		((gwiz.map.tile[gwiz.x][gwiz.y] & NWALL) != NWALL))
		{
		    gwiz.y--;
		    moved = TRUE;
		}
	    else if ((direction == 1) &&
		     ((gwiz.map.tile[gwiz.x][gwiz.y] & NWALL) == NWALL))
		MsgBox("Ouch!");
	    break;
	}
    return (moved);
}

char Act(void)
{
    /* I considered other ways than "doorinvolved," but ultimately, this
       seemed like a reasonable solution at the time.  It works, and it's
       easy to read. */
    short x = gwiz.x;
    short y = gwiz.y;
    char moved = FALSE;

    switch (gwiz.face)
	{
	case NORTH:
	    if (NDOOR & gwiz.map.tile[x][y])
		gwiz.y--;
	    else if (NWALL & gwiz.map.tile[x][y])
		{
		    MsgBox ("Ouch!");
		    break;
		}
	    else
		gwiz.y--;
	    break;
	case EAST:
	    if (EDOOR & gwiz.map.tile[x][y])
		gwiz.x++;
	    else if (EWALL & gwiz.map.tile[x][y])
		{
		    MsgBox ("Ouch!");
		    break;
		}
	    else
		gwiz.x++;
	    break;
	case SOUTH:
	    if (SDOOR & gwiz.map.tile[x][y])
		gwiz.y++;
	    else if (SWALL & gwiz.map.tile[x][y])
		{
		    MsgBox ("Ouch!");
		    break;
		}
	    else
		gwiz.y++;
	    break;
	case WEST:
	    if (WDOOR & gwiz.map.tile[x][y])
		gwiz.x--;
	    else if (WWALL & gwiz.map.tile[x][y])
		{
		    MsgBox ("Ouch!");
		    break;
		}
	    else
		gwiz.x--;
	    break;
	}

    if ((gwiz.x != x) || (gwiz.y != y))
	moved = TRUE;
    return (moved);
}

int WaitForAnyKey (void)
{
    SDL_Event event;
    
    while (SDL_WaitEvent (&event) != 0)
	{
	    switch (event.type)
		{
		case SDL_JOYBUTTONDOWN:
		    return event.jbutton.button;
		case SDL_JOYAXISMOTION:
		    if (gwiz.joy.enabled)
			return -1;
		    break;
		case SDL_KEYDOWN:
		    return event.key.keysym.sym;
		case SDL_QUIT:
		    VerifyQuit();
		    break;
		}
	}
    return -1;
}

int EventIsUp (SDL_Event *event)
{
    SDLKey sym = event->key.keysym.sym;
    switch (event->type)
	{
	case SDL_KEYDOWN:
	    if ((sym == SDLK_UP) || (sym == gwiz.key.fwd))
		return (TRUE);
	    break;
	case SDL_JOYAXISMOTION:
	    if (gwiz.joy.enabled)
		if (JoyAxisMotion (event, NORTH))
		    return (TRUE);
	    break;
	}
    return (FALSE);
}

int EventIsDown (SDL_Event *event)
{
    SDLKey sym = event->key.keysym.sym;
    switch (event->type)
	{
	case SDL_KEYDOWN:
	    if ((sym == SDLK_DOWN) || (sym == gwiz.key.bkwd))
		return (TRUE);
	    break;
	case SDL_JOYAXISMOTION:
	    if (gwiz.joy.enabled)
		if (JoyAxisMotion (event, SOUTH))
		    return (TRUE);
	    break;
	}
    return (FALSE);
}

int EventIsLeft (SDL_Event *event)
{
    SDLKey sym = event->key.keysym.sym;
    switch (event->type)
	{
	case SDL_KEYDOWN:
	    if ((sym == SDLK_LEFT) || (sym == gwiz.key.lft))
		return (TRUE);
	    break;
	case SDL_JOYAXISMOTION:
	    if (gwiz.joy.enabled)
		if (JoyAxisMotion (event, WEST))
		    return (TRUE);
	    break;
	}
    return (FALSE);
}

int EventIsRight (SDL_Event *event)
{
    SDLKey sym = event->key.keysym.sym;
    switch (event->type)
	{
	case SDL_KEYDOWN:
	    if ((sym == SDLK_RIGHT) || (sym == gwiz.key.rgt))
		return (TRUE);
	    break;
	case SDL_JOYAXISMOTION:
	    if (gwiz.joy.enabled)
		if (JoyAxisMotion (event, EAST))
		    return (TRUE);
	    break;
	}
    return (FALSE);
}

int EventIsOk (SDL_Event *event)
{
    SDLKey sym = event->key.keysym.sym;
    switch (event->type)
	{
	case SDL_KEYDOWN:
	    if ((sym == SDLK_RETURN) || (sym == gwiz.key.act))
		return (TRUE);
	    break;
	case SDL_JOYBUTTONDOWN:
	    CheckJoy();
	    if (gwiz.joy.enabled)
		if (GetJoyButton (event) == gwiz.joy.act)
		    return (TRUE);
	    break;
	}
    return (FALSE);
}

int EventIsCancel (SDL_Event *event)
{
    SDLKey sym = event->key.keysym.sym;
    switch (event->type)
	{
	case SDL_KEYDOWN:
	    if ((sym == SDLK_ESCAPE) || (sym == gwiz.key.cancel))
		return (TRUE);
	    break;
	case SDL_JOYBUTTONDOWN:
	    CheckJoy();
	    if (gwiz.joy.enabled)
		if (GetJoyButton (event) == gwiz.joy.cancel)
		    return (TRUE);
	    break;
	}
    return (FALSE);
}

int EventIsStepLeft (SDL_Event *event)
{
    SDLKey sym = event->key.keysym.sym;
    switch (event->type)
	{
	case SDL_KEYDOWN:
	    if (sym == gwiz.key.slft)
		return (TRUE);
	    break;
	case SDL_JOYBUTTONDOWN:
	    CheckJoy();
	    if (gwiz.joy.enabled)
		if (GetJoyButton (event) == gwiz.joy.slft)
		    return (TRUE);
	    break;
	}
    return (FALSE);
}

int EventIsStepRight (SDL_Event *event)
{
    SDLKey sym = event->key.keysym.sym;
    switch (event->type)
	{
	case SDL_KEYDOWN:
	    if (sym == gwiz.key.srgt)
		return (TRUE);
	    break;
	case SDL_JOYBUTTONDOWN:
	    CheckJoy();
	    if (gwiz.joy.enabled)
		if (GetJoyButton (event) == gwiz.joy.srgt)
		    return (TRUE);
	    break;
	}
    return (FALSE);
}

int EventIsStepBack (SDL_Event *event)
{
    SDLKey sym = event->key.keysym.sym;
    switch (event->type)
	{
	case SDL_KEYDOWN:
	    if (sym == gwiz.key.bkwd)
		return (TRUE);
	    break;
	case SDL_JOYBUTTONDOWN:
	    CheckJoy();
	    if (gwiz.joy.enabled)
		if (GetJoyButton (event) == gwiz.joy.bkwd)
		    return (TRUE);
	    break;
	}
    return (FALSE);
}

int EventIsFta (SDL_Event *event)
{
    SDLKey sym = event->key.keysym.sym;
    switch (event->type)
	{
	case SDL_KEYDOWN:
	    if (sym == gwiz.key.fta)
		return (TRUE);
	    break;
	case SDL_JOYAXISMOTION:
	    if (gwiz.joy.enabled)
		if (JoyAxisMotion (event, SOUTH))
		    return (TRUE);
	    break;
	}
    return (FALSE);
}

/* "extra" stuff to check in event loops.  Don't forget me. */
int EventIsMisc (SDL_Event *event)
{
    char acted = FALSE;
    switch (event->type)
	{
	case SDL_KEYDOWN:
	    if (event->key.keysym.sym == SDLK_RETURN)
		if (KMOD_ALT & event->key.keysym.mod)
		    {
			SDL_Surface *tmp = gwiz.canvas;
			if ((gwiz.vidmode & SDL_FULLSCREEN) == SDL_FULLSCREEN)
			    gwiz.vidmode = (gwiz.vidmode^SDL_FULLSCREEN);
			else
			    gwiz.vidmode = (gwiz.vidmode|SDL_FULLSCREEN);
			
			gwiz.canvas = SDL_SetVideoMode (800, 600, gwiz.bpp,
							gwiz.vidmode);
			if (!gwiz.canvas)
			    {
				GErr ("Unable to change video mode: %s",
					  SDL_GetError());
			    }
			SDL_BlitSurface (tmp, NULL, gwiz.canvas, NULL);
			SDL_Flip (gwiz.canvas);
			SDL_FreeSurface (tmp);

			acted = TRUE;
		    }
#ifdef GWIZ_CHEATING
	    if (event->key.keysym.sym == SDLK_F10)
		{
		    char epc[16];

		    NewTextEntry ("Please enter desired EP", epc, 16);

		    gwiz.pawn[0].counter.ep = atoi (epc);
		}
#endif /* GWIZ_CHEATING */
	    if (event->key.keysym.sym == gwiz.key.quit)
		GwizShutdown (0);
	    break;
	case SDL_QUIT:
	    VerifyQuit();
	    break;
	}
    return acted;
}
