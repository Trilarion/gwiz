/*  shopnode.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_SHOPNODE_H
#define HAVE_SHOPNODE_H

typedef struct GwizShopNode_ GwizShopNode;

struct GwizShopNode_ {
    GwizShopNode *p;
    GwizShopNode *n;
    char name[16];
    int price;
    int stock;
    int itemno;
};

/* Populate the shop lists with NULL values ("no node mode") */
void InitShopLists (void);

/* Create a new node */
GwizShopNode *NewGwizShopNode (char *name, int price, int stock, int itemno);

/* Add a new item to the end of the list */
void AppendGwizShopNode (GwizShopNode **gsn, GwizShopNode *newnode);

/* Add a new item to the front of the list */
void PrependGwizShopNode (GwizShopNode **gsn, GwizShopNode *newnode);

/* Delete a specific node */
void DeleteGwizShopNode (GwizShopNode *gsn, int nodeno);

/* Delete ALL nodes in a list */
void DestroyShopNodeList (GwizShopNode **gsn);

/* Get the number of nodes linked by a given node */
int CountShopNodes (GwizShopNode *gsn);

/* Return the name a given node number */
char *GetNodeName (GwizShopNode *gsn, int nodeno);

/* Return the price or stock availability of a given node number */
int GetNodePrice (GwizShopNode *gsn, int nodeno);

/* Return the availability of the given node number */
int GetNodeStock (GwizShopNode *gsn, int nodeno);

int GetNodeItem (GwizShopNode *gsn, int nodeno);

void SetNodeStock (GwizShopNode *gsn, int nodeno, int increment);

#endif
