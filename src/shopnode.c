/*  shopnode.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <string.h>
#include "gwiz.h"

extern GwizApp gwiz;

void InitShopLists (void)
{
    gwiz.shop.wpn = NULL;
    gwiz.shop.arm = NULL;
    gwiz.shop.shld = NULL;
    gwiz.shop.hlm = NULL;
    gwiz.shop.gnt = NULL;
    gwiz.shop.misc = NULL;
    gwiz.shop.scrl = NULL;

    gwiz.shop.wpncount = 0;
    gwiz.shop.armcount = 0;
    gwiz.shop.shldcount = 0;
    gwiz.shop.hlmcount = 0;
    gwiz.shop.gntcount = 0;
    gwiz.shop.misccount = 0;
    gwiz.shop.scrlcount = 0;
}

/* Create a new node */
GwizShopNode *NewGwizShopNode (char *name, int price, int stock, int itemno)
{
    GwizShopNode *node = NULL;
    
    node = Smalloc (sizeof(GwizShopNode));
    
    node->p = NULL;
    node->n = NULL;
    strncpy (node->name, name, 16);
    node->name[15] = '\0'; /* make sure the result was null terminated */
    node->price = price;
    node->stock = stock;
    node->itemno = itemno;
    
    return (node);
}

/* Add a new item to the end of the list */
void AppendGwizShopNode (GwizShopNode **gsn, GwizShopNode *newnode)
{
    GwizShopNode *curnode = *gsn;

    if (*gsn == NULL)
	{
	    *gsn = newnode;
	    return;
	}
    
    while (curnode->n != NULL)
	curnode = curnode->n;
    
    newnode->p = curnode;
    curnode->n = newnode;
}

/* Add a new item to the front of the list */
void PrependGwizShopNode (GwizShopNode **gsn, GwizShopNode *newnode)
{
    GwizShopNode *curnode = *gsn;

    if (*gsn == NULL)
	{
	    *gsn = newnode;
	    return;
	}
    
    while (curnode->p != NULL)
	curnode = curnode->p;
    
    newnode->n = curnode;
    curnode->p = newnode;
}

/* Delete a specific node */
void DeleteGwizShopNode (GwizShopNode *gsn, int nodeno)
{
    GwizShopNode *curnode = gsn;
    int i = 0;

    if (gsn == NULL)
	return;

    while (curnode->p != NULL)
	curnode = curnode->p; /* reset the list to the beginning */
    
    for (i = 0; i < nodeno+1; i++);
    {
	if (curnode->n == NULL)
	    return;
	else if (i == nodeno)
	    {
		/* only link existing elements together */
		if (gsn->n != NULL)
		    gsn->n->p = gsn->p;
		if (gsn->p != NULL)
		    gsn->p->n = gsn->n;
		Sfree (gsn->name);
		Sfree (gsn);
		return;
	    }
	curnode = curnode->n;
    }
}

/* Delete ALL nodes in a list */
void DestroyShopNodeList (GwizShopNode **gsn)
{
    int nodecount;
    int i;

    if (*gsn == NULL)
	return;

    nodecount = CountShopNodes (*gsn);
    
    for (i = 0; i < nodecount; i++)
	/* Only delete node 0 because elements positions are shifted */
	DeleteGwizShopNode (*gsn, 0);
    *gsn = NULL;
}

/* Get the number of nodes linked by a given node */
int CountShopNodes (GwizShopNode *gsn)
{
    GwizShopNode *curnode = gsn;
    int nodecount = 0;
    
    while (curnode->p != NULL)
	curnode = curnode->p; /* reset list to the beginning */
    
    while (curnode->n != NULL)
	{
	    curnode = curnode->n;
	    nodecount++;
	}
    
    return (nodecount);
}

/* Get the name field in a given node */
char *GetNodeName (GwizShopNode *gsn, int nodeno)
{
    GwizShopNode *curnode = gsn;
    int i;

    if (gsn == NULL)
	return ("NODE_NOEXIST");

    while (curnode->p != NULL)
	curnode = curnode->p;
    
    for (i = 0; i < nodeno +1; i++)
	{
	    if (i == nodeno)
		return (curnode->name);
	    if (curnode->n != NULL)
		curnode = curnode->n;
	    else
		return (curnode->name);
	}
    return (curnode->name);
}

/* Return the price or stock availability of a given node number */
int GetNodePrice (GwizShopNode *gsn, int nodeno)
{
    GwizShopNode *curnode = gsn;
    int i;

    if (gsn == NULL)
	return (0);

    while (curnode->p != NULL)
	curnode = curnode->p;
    
    for (i = 0; i < nodeno +1; i++)
	{
	    if (i == nodeno)
		return (curnode->price);
	    if (curnode->n != NULL)
		curnode = curnode->n;
	    else
		return (curnode->price);
	}
    return (curnode->price);
}

/* Return the availability of the given node number */
int GetNodeStock (GwizShopNode *gsn, int nodeno)
{
    GwizShopNode *curnode = gsn;
    int i;

    if (gsn == NULL)
	return (0);

    while (curnode->p != NULL)
	curnode = curnode->p;
    
    for (i = 0; i < nodeno +1; i++)
	{
	    if (i == nodeno)
		return (curnode->stock);
	    if (curnode->n != NULL)
		curnode = curnode->n;
	    else
		return (curnode->stock);
	}
    return (curnode->stock);
}

int GetNodeItem (GwizShopNode *gsn, int nodeno)
{
    GwizShopNode *curnode = gsn;

    int i;

    if (gsn == NULL)
	return (0);
    while (curnode->p != NULL)
	curnode = curnode->p;

    for (i = 0; i < nodeno; i++)
	{
	    if (i == nodeno)
		return (curnode->itemno);
	    if (curnode->n != NULL)
		curnode = curnode->n;
	    else 
		return (curnode->itemno);
	}
    return (curnode->itemno);
}

void SetNodeStock (GwizShopNode *gsn, int nodeno, int increment)
{
    GwizShopNode *curnode = gsn;
    int i;

    if (gsn == NULL)
	return;

    while (curnode->p != NULL)
	curnode = curnode->p;
    
    for (i = 0; i < nodeno +1; i++)
	{
	    if (i == nodeno)
		{
		    if (curnode->stock == -1) /* can't affect perm. items */
			return;
		    if (increment)
			curnode->stock++;
		    else
			curnode->stock--;
		    return;
		}
	    if (curnode->n != NULL)
		curnode = curnode->n;
	    else
		return;
	}
    return;
}
