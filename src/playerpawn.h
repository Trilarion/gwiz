/*  playerpawn.h: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef HAVE_PLAYERPAWN_H
#define HAVE_PLAYERPAWN_H

typedef struct GwizPawnList_ GwizPawnList;
typedef struct AttrLimits_ AttrLimits;

struct GwizPawnList_ {
    SDL_Surface *displace;
    SDL_Surface *area;
    SDL_Surface *names;
    SDL_Rect arect;
    SDL_Rect crect;
    int maxposition;
    int position;
};

/* set up common conditions for blitting */
void InitStatusPix(void);

/* Create new, blank player.  Zero all data fields. */
void InitNewPlayer (PlayerPawn *pawn);

/* Verify ~/.gwiz/party exists, and is valid (by version number) */
void ValidatePartyFile(void);

/* Create ~/.gwiz/party */
void CreatePartyFile (void);

void SavePawn (PlayerPawn pawn, int whichpawn);

void LoadPawn (int whichpawn, PlayerPawn *pawn);

void DeletePawn (void);

void CompactPartyFile (void);

/* Flip the pawn's attributes to network-endianness and back again */
void FlipPawnToSaveFormat (PlayerPawn *pawn);
void FlipPawnToPlayFormat (PlayerPawn *pawn);

/* Return the number of a pawn we can use to save a new one. */
int FirstAvailablePawn (void);

void GetClassAbbr(int class, char *cname);

int NewPawnList(void);

void MovePawnListCursor(GwizPawnList *gpl, int direction);

SDL_Surface *PawnListing(int which);

void LoadPawnListPix (GwizPawnList *gpl);

/* pass a pawn number, return value is the location of that pawn in the file */
int GetLiteralPawnNo (int which);

int CountValidPawns(void);

SDL_Surface *RenderPawnNames (PlayerPawn *pawn);

SDL_Surface *RenderPawnSexes (PlayerPawn *pawn);

SDL_Surface *RenderPawnAlis (PlayerPawn *pawn);

SDL_Surface *RenderPawnClasses(PlayerPawn *pawn);

SDL_Surface *RenderPawnACs (PlayerPawn *pawn);

SDL_Surface *RenderPawnHPs (PlayerPawn *pawn);

SDL_Surface *RenderPawnHPMaxes(PlayerPawn *pawn);

int CheckPawnUsage (int which);

void PoolGold (int pawnno);

void DivvyGold (void);

SDL_Surface *RenderPawnInventory (PlayerPawn *pawn);

void ChangeClass (void);

void RenamePawn (void);

void ReorderPartyFile (void);

short int PawnIsMage (PlayerPawn *pawn);

short int PawnIsCleric (PlayerPawn *pawn);

short int PawnIsWizard (PlayerPawn *pawn);

short int PawnIsNotMagical (PlayerPawn *pawn);

short int PartyIsGood (void);

short int PartyIsNeutral (void);

short int PartyIsEvil (void);

short int PawnIsPartyCompatible (PlayerPawn *pawn);

short int GetNumberOfIncompatiblePawns (void);

#endif /* HAVE_PLAYERPAWN_H */



