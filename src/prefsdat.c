/*  prefsdat.c: (c) 2002 sibn

    This file is part of GWiz.

    GWiz is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    GWiz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GWiz; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "SDL_thread.h"
#include "gwiz.h"
#include "text.h"
#include "uiloop.h"
#include "playerpawn.h"
#include "prefsdat.h"
#include "keynames.h"
#include "conf.h"

extern GwizApp gwiz;

void InitPrefs (int argc, char **argv)
{
    int i;

    gwiz.cond.lumen = 0;
    gwiz.cond.volitare = 0;
    gwiz.cond.agnoscere = 0;

    gwiz.font.ptsize = 16;
    gwiz.font.aa = TRUE;
    gwiz.udata.home = getenv ("HOME");
    gwiz.udata.gwiz = MakePath (gwiz.udata.home, "/.gwiz");
    gwiz.udata.cfg = MakePath (gwiz.udata.gwiz, "/gwiz.ini");
    gwiz.udata.party = MakePath (gwiz.udata.gwiz, "/party");
    gwiz.udata.monsters = PKGDATADIR "/monsters";
    gwiz.udata.items = PKGDATADIR "/items";
    {
	/* Check to make sure the prefs dir exists */
	int dd = open (gwiz.udata.gwiz, O_RDONLY);
	if (dd < 0)
	{
	    if (mkdir (gwiz.udata.gwiz, 0755) < 0)
		GErr ("Unable to create basedir %s", gwiz.udata.gwiz);
	    dd = open (gwiz.udata.gwiz, O_RDONLY);
	}
	if (dd < 0)
	    GErr ("Unable to create basedir %s", gwiz.udata.gwiz);
	close (dd);
    }

    gwiz.key.quit = 0;
    gwiz.key.fwd = SDLK_KP8;
    gwiz.key.bkwd = SDLK_KP5;
    gwiz.key.lft = SDLK_KP7;
    gwiz.key.rgt = SDLK_KP9;
    gwiz.key.slft = SDLK_KP4;
    gwiz.key.srgt = SDLK_KP6;
    gwiz.key.fta = SDLK_KP2;
    gwiz.key.act = SDLK_KP_ENTER;
    gwiz.key.cancel = SDLK_KP0;

    gwiz.joy.enabled = TRUE;
    gwiz.joy.whichdev = 0;
    gwiz.joy.fwd = -16384;
    gwiz.joy.lft = -16384;
    gwiz.joy.rgt = 16384;
    gwiz.joy.fta = 16384;
    gwiz.joy.bkwd = -1;
    gwiz.joy.slft = -1;
    gwiz.joy.srgt = -1;
    gwiz.joy.act = 0;
    gwiz.joy.cancel = 1;

    gwiz.vidmode = 0;
#if SDL_BYTE_ORDER == SDL_BIGENDIAN
    gwiz.rmask = 0xff000000;
    gwiz.gmask = 0x00ff0000;
    gwiz.bmask = 0x0000ff00;
#else
    gwiz.rmask = 0x000000ff;
    gwiz.gmask = 0x0000ff00;
    gwiz.bmask = 0x00ff0000;
#endif
    gwiz.nmask = 0x00000000;
    gwiz.bgc = 0x00000000;
    gwiz.fgc = 0x00000000;
    gwiz.bpp = 32;
    gwiz.ffi = TRUE;
    gwiz.zaa = TRUE;

    for (i = 0; i < 6; i++)
	gwiz.pawnno[i] = -1;
}

void LoadPrefs (void)
{
    OpenConf (gwiz.udata.cfg);
    /*    INI *pf;

    gwiz.pf = NewINI (gwiz.udata.cfg, PKGDATADIR "/gwiz.ini", errhandle);
    pf = gwiz.pf;

    if (GetSettingBool (pf, "video", "fullscreen"))
	gwiz.vidmode |= SDL_FULLSCREEN;
    if (GetSettingBool (pf, "video", "hwrender"))
	gwiz.vidmode |= SDL_HWSURFACE;
    gwiz.bpp = GetSettingInt (pf, "video", "depth");
    if (GetSettingBool (pf, "video", "doublebuf"))
	gwiz.vidmode |= SDL_DOUBLEBUF;
    gwiz.zaa = GetSettingBool (pf, "video", "zoom_aa");
    gwiz.font.ptsize = GetSettingInt (pf, "font", "size");
    gwiz.font.aa = GetSettingBool (pf, "font", "aa");

    tmp = GetSetting (pf, "keys", "quit");
    gwiz.key.quit = CharToKey (tmp, 0);
    Sfree (tmp);

    tmp = GetSetting (pf, "keys", "ok");
    gwiz.key.act = CharToKey (tmp, SDLK_KP8);
    Sfree (tmp);

    tmp = GetSetting (pf, "keys", "cancel");
    gwiz.key.cancel = CharToKey (tmp, SDLK_KP0);
    Sfree (tmp);

    tmp = GetSetting (pf, "keys", "forward");
    gwiz.key.fwd = CharToKey (tmp, SDLK_KP8);
    Sfree (tmp);

    tmp = GetSetting (pf, "keys", "backward");
    gwiz.key.bkwd = CharToKey (tmp, SDLK_KP5);
    Sfree (tmp);

    tmp = GetSetting (pf, "keys", "left");
    gwiz.key.lft = CharToKey (tmp, SDLK_KP7);
    Sfree (tmp);

    tmp = GetSetting (pf, "keys", "right");
    gwiz.key.rgt = CharToKey (tmp, SDLK_KP9);
    Sfree (tmp);

    tmp = GetSetting (pf, "keys", "aboutface");
    gwiz.key.fta = CharToKey (tmp, SDLK_KP2);
    Sfree (tmp);

    tmp = GetSetting (pf, "keys", "stepleft");
    gwiz.key.slft = CharToKey (tmp, SDLK_KP4);
    Sfree (tmp);

    tmp = GetSetting (pf, "keys", "stepright");
    gwiz.key.srgt = CharToKey (tmp, SDLK_KP6);
    Sfree (tmp);
    */

}

void SavePrefs (void)
{
    SaveConf (gwiz.udata.cfg);
    /*
    INI *pf = gwiz.pf;

    SetSetting (pf, "video", "fullscreen", FMT_STRING, 
		(SDL_FULLSCREEN&gwiz.vidmode) ? "true" : "false");
    SetSetting (pf, "video", "hwrender", FMT_STRING,
		(SDL_HWSURFACE&gwiz.vidmode) ? "true" : "false");
    SetSetting (pf, "video", "depth", FMT_INT, &gwiz.bpp);
    SetSetting (pf, "video", "doublebuf", FMT_STRING,
		(SDL_DOUBLEBUF&gwiz.vidmode) ? "true" : "false");
    SetSetting (pf, "video", "zoom_aa", FMT_STRING, 
		gwiz.zaa ? "true" : "false");
    SetSetting (pf, "font", "ptsize", FMT_INT, &gwiz.font.ptsize);
    SetSetting (pf, "keys", "quit", FMT_STRING, KeyToChar (gwiz.key.quit));
    SetSetting (pf, "keys", "forward", FMT_STRING, KeyToChar (gwiz.key.fwd));
    SetSetting (pf, "keys", "backward", FMT_STRING, KeyToChar (gwiz.key.bkwd));
    SetSetting (pf, "keys", "left", FMT_STRING, KeyToChar (gwiz.key.lft));
    SetSetting (pf, "keys", "right", FMT_STRING, KeyToChar (gwiz.key.rgt));
    SetSetting (pf, "keys", "stepright", FMT_STRING, KeyToChar(gwiz.key.srgt));
    SetSetting (pf, "keys", "stepleft", FMT_STRING, KeyToChar (gwiz.key.slft));
    SetSetting (pf, "keys", "ok", FMT_STRING, KeyToChar (gwiz.key.act));
    SetSetting (pf, "keys", "cancel", FMT_STRING, KeyToChar (gwiz.key.cancel));
    */
    /*    SetSetting (pf, "joystick", "enabled", FMT_STRING,
		gwiz.joy.enabled ? "true" : "false");
    SetSetting (pf, "joystick", "devicenumber", FMT_INT, &gwiz.joy.whichdev);
    SetSetting (pf, "joystick", "forward", FMT_INT, &gwiz.joy.fwd);
    SetSetting (pf, "joystick", "backward", FMT_INT, &gwiz.joy.bkwd);
    SetSetting (pf, "joystick", "left", FMT_INT, &gwiz.joy.lft);
    SetSetting (pf, "joystick", "right", FMT_INT, &gwiz.joy.rgt);
    SetSetting (pf, "joystick", "aboutface", FMT_INT, &gwiz.joy.fta);
    SetSetting (pf, "joystick", "stepleft", FMT_INT, &gwiz.joy.slft);
    SetSetting (pf, "joystick", "stepright", FMT_INT, &gwiz.joy.srgt);
    SetSetting (pf, "joystick", "ok", FMT_INT, &gwiz.joy.act);
    SetSetting (pf, "joystick", "cancel", FMT_INT, &gwiz.joy.cancel); */
    /*
    {
	PSect *sect = pf->list;
	while (sect)
	    {
		PNode *node = sect->node;
		printf ("%s\n", sect->name);
		while (node)
		    {
			printf ("%s\n", node->line);
			node = node->next;
		    }
		sect = sect->next;
	    }		
    }

    SaveINI (pf);
    */
}



